CSE 407 Project Pipeline

The entire process is scripted. You can run all the steps here in order by executing
    > sh runFull.sh

Instructions for all stages of obtaining antibody attributes from fpocket and compiling
data to correlate.

1. Update pdb_list.txt with all proteins you wish to process.

2. run getPdb.sh. This will fetch all proteins from pdb_list.txt and save them in pdb/
    > sh getPdb.sh

3. go to pdb_chain/ and run sepPdb.sh. This removes all antigens and ligands from the
 files stored in the pdb/ directory.
    > cd pdb_chain
    > sh sepPdb.sh

4. go back to the parent directory and run run_fpocket.sh. You need to specify the folder
 with the source .pdb files and the output directory
    > cd ..
    > sh run_fpocket.sh pdb_chain/no_antibodies pocket_no_antibodies

5. go to pocket_no_antibodies and run extractData.sh. This script uses the pocket matches
 in pocket_matches.txt to extract data from the fpocket output and place it in the correct
 folder.
    > cd pocket_no_antibodies
    > sh extractData.sh


Then you should be done!
