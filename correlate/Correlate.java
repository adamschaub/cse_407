/** CSE 411 Homework 1
 *
 * @author Adam Schaub
 */

package c;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/** Correlate
 * The main class for Homework 1. When running, include the path to the data files
 * to be read as input in the command line. E.g. java hw1.Correlate a.dat b.dat
 * Each file will be parsed for all data points and evaluated using three different
 * correlation measurements: Pearson product-moment, Spearman rank, and Kendall tau rank.
 */
class Correlate {

	/** double correlatePearson(ArrayList<Double>, ArrayList<Double>)
	 * This function accepts to arrays of corresponding x and y values and
	 * assumes the lists to be of equivalent lengths. Returns the Pearson's product-
	 * moment correlation coefficient for the given data. Will return NaN for bad
	 * data sets.
	 *
	 * @param x	ArrayList of x data points
	 * @param y 	ArrayList of y data points
	 * @return	The correlation coefficient
	 */
	static double correlatePearson(ArrayList<Double> x, ArrayList<Double> y) {
		//could assert x.size() == y.size(), to be safe
		double mX, mY;
		mX = mean(x);
		mY = mean(y);

		double covS, covX, covY;
		covS = 0;
		covX = 0;
		covY = 0;
		//loop through each, x and y sizes are the same
		for(int i = 0; i < x.size(); ++i) {
			covS += (x.get(i)-mX)*(y.get(i)-mY);
			covX += (x.get(i)-mX)*(x.get(i)-mX);
			covY += (y.get(i)-mY)*(y.get(i)-mY);
		}
		covX = Math.sqrt(covX);
		covY = Math.sqrt(covY);

		return covS/(covX*covY);
	}

	/** double correlateSpearman(PointList)
	 * This function requires a PointList which has been pre-sorted
	 * in both the x and y dimensions. Will return NaN for bad
	 * data sets.
	 *
	 * @param x	ArrayList of x data points
	 * @param y 	ArrayList of y data points
	 * @return	The correlation coefficient using Spearman's rank correlation
	 */
	static double correlateSpearman(PointList pl) {
		double total = 0;
		for(Point p : pl.list) {
			total += (p.xRank - p.yRank)*(p.xRank - p.yRank);
		}
		int n = pl.list.size();
		return 1 - 6*total/(n*(n*n-1));
	}


	/** double correlateKendall(PointList)
	 * This function requires a PointList which has been pre-sorted
	 * in both the x and y dimensions. The PointList MUST contain only
	 * unique data sets for x and y data (separate sets). This can be
	 * checked using the isUnique flag of the PointList after sorting
	 * and ranking all elements. Will return NaN for bad
	 * data sets.
	 *
	 * @param pl 	PointList of all data to be correlated
	 * @return	The correlation coefficient using Kendall's tau rank correlation
	 */
	static double correlateKendall(PointList pl) {
		ArrayList<Point> pts = pl.list;
		int n = pts.size();
		int concordant, discordant;
		concordant = 0;
		discordant = 0;
		for(int i = 0; i < n; ++i) {
			for(int j = i+1; j < n; ++j) {
				//not checking i == j ever, so need not check for matching rank
				if(((pts.get(i).xRank > pts.get(j).xRank) && (pts.get(i).yRank > pts.get(j).yRank)) || ((pts.get(i).xRank < pts.get(j).xRank) && (pts.get(i).yRank < pts.get(j).yRank))) {
					concordant++;
				}
				else {
					discordant++;
				}
			}
		}

		return (concordant - discordant)/(0.5*n*(n-1));
	}

	/** double mean(ArrayList<Double>)
	 * Simple mean of all elements in an ArrayList.
	 *
	 * @param l	ArrayList<Double> of values to average
	 * @return 	the mean of all elements in l
	 */
	static double mean(ArrayList<Double> l) {
		double sum = 0;
		for(Double d : l) {
			sum += d;
		}
		return sum/l.size();
	}

	/** void printUsage()
	 * Prints the usage for Correlate. Called if used incorrectly.
	 */
	static void printUsage() {
		System.out.println("Usage: java hw1.Correlate [FILE...]");
	}

	/** void doCorrelate(File)
	 * Parses all data points from the given file, appropriately warning
 	 * for bad format and missing files. Places all points into appropriate
	 * data structures to be passed to each of the correlation methods.
	 *
	 * @param f	File to be parsed
	 */
	static void doCorrelate(File f) {
		Scanner in = null;
		try {
			in = new Scanner(f);
		} catch(FileNotFoundException e) {
			System.out.println("Could not open " + f.getAbsolutePath() + ". No such file or directory");
			return; //abort this method
		}

		//list for data from each paired trial
		ArrayList<Double> l1, l2;
		l1 = new ArrayList<Double>();
		l2 = new ArrayList<Double>();

		int lineCount = 0;
		while(in.hasNextLine()) {
			lineCount++;
			StringTokenizer st = new StringTokenizer(in.nextLine());
			if(st.countTokens() != 2) {
				System.out.println("Bad format at " + f.getName() + ":" + lineCount + ". Skipping.");
				continue;
			}

			double d1, d2;
			String s1, s2;
			s1 = st.nextToken();
			s2 = st.nextToken();

			try {
				d1 = Double.parseDouble(s1);
				d2 = Double.parseDouble(s2);
			} catch(Exception e) {
				System.out.println("Unable to parse data at " + f.getName() + ":" + lineCount + ". Skipping.");
				continue;
			}

			l1.add(d1);
			l2.add(d2);
		}

		//structure for ranking data points
		PointList pl = new PointList(l1, l2);
		pl.sortAndRankY();
		pl.sortAndRankX();

		System.out.println("Correlation coefficients for " + f.getName());
		System.out.printf("\tPearson's: %.4f\n", correlatePearson(l1, l2));
		System.out.printf("\tSpearman's: %.4f\n", correlateSpearman(pl));
		//Kendall's correlation cannot be performed on non-unique sets.
		//	System.out.printf("\tKendall's: %.4f\n", correlateKendall(pl));
		//	System.out.println("\tKendalls's: Cannot be applied (non-unique data set)");
	}

	public static void main(String[] args) {
		ArrayList<File> files = new ArrayList<File>();

		if(args.length < 1) printUsage();
		for(String fName : args) {
			System.out.println("<--------------------" + fName + "-------------------->");
			doCorrelate(new File(fName));
		}


	}
}
