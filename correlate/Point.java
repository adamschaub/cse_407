/** CSE 411 Homework 411
 *
 * @author Adam Schaub
 */

package c;

/**
 * Point is a basic class for holding data pairs. Each
 * Point has an yRank and xRank to be set when sorted
 * into a list of Point objects.
 */
class Point{

	/** Public Fields */
	public Double x;
	public Double y;
	public Double yRank;
	public Double xRank;

	/**
	 * Basic constructor for Point object. The x and y ranks
	 * are set to zero and should be set when sorting.
	 *
	 * @param x	the x dimension of the data point
	 * @param y	the y dimension of the data point
	 */
	public Point(Double x, Double y) {
		this.x = x;
		this.y = y;
		this.yRank = (double)0;
		this.xRank = (double)0;
	}
}
