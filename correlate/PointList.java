/** CSE 411 Homework 411
 *
 * @author Adam Schaub
 */

package c;

import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;

/**
 * PointList is a simple wrapper for an array of Point objects. Implemented
 * to make sorting and ranking points centralized.
 */
class PointList {

	/** Public Fields */
	public ArrayList<Point> list;
	/** isUnique
	 * A boolean flag indicating whether the set of points contains
	 * any duplicate data in either the x or y dimension. It is initially
	 * set to true and calling sortAndRankX or sortAndRankY will set it
	 * to false if there is non-unique data in either the set of all x or
	 * the set of all y values.
	 */
	public boolean isUnique;

	/**
	 * Sole constructor for PointList. Sequentially adds new points
	 * using the next element from x and y. If x is larger than y or
	 * vice versa, the smaller of the two is used as the size limit.
	 *
	 * @param x	ArrayList<Double> of x data points
	 * @param y	ArrayList<Double> of y data points
	 */
	public PointList(ArrayList<Double> x, ArrayList<Double> y) {
		int size = x.size() < y.size()? x.size():y.size();
		list = new ArrayList<Point>();
		for(int i = 0; i < size; ++i) {
			this.add(x.get(i), y.get(i));
		}
		isUnique = true;
	}

	/**
	 * add(Double, Double) adds a new point to list with the specified
	 * dimensions.
	 *
	 * @param x	x dimension of the data point
	 * @param y	y dimension of the data point
	 */
	public void add(Double x, Double y) {
		this.add(new Point(x, y));
	}

	/**
	 * add(Point) adds the given point to the list.
	 *
	 * @param p	Point to add to the list
	 */
	public void add(Point p) {
		list.add(p);
	}

	/**
	 * sortAndRankX() sorts and ranks all points according to their
	 * x dimension. If any number of points share an x dimension, the
	 * rank is averaged across those points, and the isUnique flag is
	 * set to false, indicating the data set contains non-unique data.
	 */
	public void sortAndRankX() {
		Collections.sort(list, new Comparator<Point>() {
			public int compare(Point p1, Point p2) {
				if(p1.x < p2.x) return -1;
				return (p1.x > p2.x) ? 1:0;
			}
		});



		int rank = 1;
		int same = 1;
		//each iteration, the  number of elements ranked is stored in "same"
		for(int i = 0; i < list.size(); i+=same) {
			same = 1; //one element to begin with
			while(((same+i) < list.size()) && list.get(i).x.equals(list.get(i+same).x)) {
				same++; //increment if following element matches value
				isUnique = false; //data in list is not unique
			}

			double avgRank = 0;
			for(int j = 0; j < same; ++j) {
				avgRank += rank++;
			}
			avgRank /= same; //get average rank
			for(int j = 0; j < same; ++j) {
				list.get(i+j).xRank = avgRank;
			}
		}
	}

	/**
	 * sortAndRankY() sorts and ranks all points according to their
	 * y dimension. If any number of points share a y dimension, the
	 * rank is averaged across those points, and the isUnique flag is
	 * set to false, indicating the data set contains non-unique data.
	 */
	public void sortAndRankY() {
		Collections.sort(list, new Comparator<Point>() {
			public int compare(Point p1, Point p2) {
				if(p1.y < p2.y) return -1;
				return (p1.y > p2.y) ? 1:0;
			}
		});

		int rank = 1;
		int same = 1;
		//each iteration, the  number of elements ranked is stored in "same"
		for(int i = 0; i < list.size(); i+=same) {
			same = 1; //one element to begin with
			while(((same+i) < list.size()) && list.get(i).y.equals(list.get(i+same).y)) {
				same++; //increment if following element matches value
				isUnique = false; //data in list is not unique
			}

			double avgRank = 0;
			for(int j = 0; j < same; ++j) {
				avgRank += rank++;
			}
			avgRank /= same; //get average rank
			for(int j = 0; j < same; ++j) {
				list.get(i+j).yRank = avgRank;
			}
		}
	}
}

