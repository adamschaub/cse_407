Author: Adam Schaub
Class: CSE 411
Assignment: Homework 1
Relevant Files: Correlate.java, PointList.java, Point.java

Run make to build all files in the current directory. Each class is in the package hw1.
All data should be placed in files and the paths passed to the class in the command line.
To run the program use 'java hw1.Correlate [FILES_PATHS...]'
	EX: java hw1.Correlate a.dat b.dat
Performs Pearson's, Spearman's, and Kendall's correlation tests on the data in each file.
Kendall's is implemented for unique data-points ONLY and will not run for data sets with
non-unique values in either column of data.
