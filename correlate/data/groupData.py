import sys

affinity_file = "affinity.dat"
attrs = ["numcavities", "volume", "surfacearea", "volsurfratio", "hydrophobicity", "alphaspheredensity"]
def group_data(prot_file):
    prot_list = prot_file.split("/")
    prot_list = prot_list[-1]
    prot_list = prot_list.split(".")
    prot_list = prot_list[0]

    print "Grouping data for " + prot_list

    try:
        f = open(prot_file, 'r')
    except:
        print "Couldn't open file " + prot_file
        return

    try:
        faff = open(affinity_file, 'r')
    except:
        print "Couldn't open affinity file " + affinity_file
        return

    #load affinites by protein name
    affs = dict()
    for line in faff:
        line = line.split()
        affs[line[0]] = float(line[1])

    for line in f:
        line = line.strip()
        fcav_str = line + "_cavity.dat"
        try:
            fcav = open(fcav_str, 'r')
        except:
            print "Can't open cavity data file for " + fcav_str
            continue
        data = fcav.readline().split()
        for i, attr in enumerate(attrs):
            fattr_str = attr + "_" + prot_list + ".dat"
            try:
                fattr = open(fattr_str, "a+")
            except:
                print "Can't open addr file " + fattr_str
                continue
            fattr.write("{0:.20f} {1}\n".format(affs[line], data[i]))

#----------MAIN----------#
if len(sys.argv) < 2:
    print 'USAGE: python groupData.py <PROT_GROUP>'
else:
    #pockets are zero-indexed when passed in
    group_data(sys.argv[1])
