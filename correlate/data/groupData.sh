#!/bin/sh

URL='http://www.rcsb.org/pdb/files/'
EXT='.txt'
REL='../../'

GS="$(< group.txt)"
for g in $GS
do
    rm *$g.dat
    python groupData.py $REL$g$EXT
done
