#!/bin/sh

URL='http://www.rcsb.org/pdb/files/'
EXT='.pdb'
DIR='pdb'

PROTS="$(< pdb_list.txt)"
rm $DIR/*$EXT
cd $DIR
for p in $PROTS
do
    CALL=$URL$p$EXT
    wget $CALL
done
