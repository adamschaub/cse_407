import sys
import getopt
import string
import os
import re
import glob


###########################################################################
### Credit
###########################################################################
def credit():
	print "pdbChainProc script, Brian Chen, Honig lab, 2009."

###########################################################################
### Usage
###########################################################################
def usage():
	credit();
	print("========================================================="   );
	print(""                                                          );
	print("python pdbChainProc.py"        );
	print("   Running with no input returns this usage info."  );
	print(""                                                          );
	print("python pdbChainProc.py -view [pdbfile(s)]"       );
	print("   Parses the PDB file(s) and outputs the chains and their extents"  );
	print("   [pdbfile(s)] can have wildcards * or ?"  );
	print(""                                                          );
	print("python pdbChainProc.py -edit [inputFile]"       );
	print("   Reads output from -view, and deletes any chain specified"  );
	print("   [inputFile] This is the stdout from -view mode"  );
	print("   The purpose of this mode is to edit the -view output manually"  );
	print("   And take out the lines you want to keep, then use this to clear"  );
	print("   the rest."  );
	print(""  );
	print("KEEP does not work right now. Only -edit."  );
	print(""  );
	print("python pdbChainProc.py -keep [inputFile]"       );
	print("   The same as -edit except that inputfile contains the chains"  );
	print("   you want to keep."  );
	print(""  );
	print("python pdbChainProc.py -filter [pdbfile(s)]"  );
	print("   Reads each PDB file and rewrites it without non-ATOM lines"  );
	print(""  );
	print("python pdbChainProc.py -unify [pdbfile(s)]"  );
	print("   Reads each PDB file and sets the chain ID to [A] "  );
	print(""  );
	print("python pdbChainProc.py -separate [outputDir] [pdbfile(s)]"  );
	print("   reads each PDB file and separates them into component chains"  );
	print("   i.e. if 1abc.pdb contains chains A,B,C, outputs: "  );
	print("   1abcA.pdb, 1abcB.pdb, 1abcC.pdb"  );
	print("   prepends the string [outputDir] before every output file name"  );
	print("========================================================="   );



###########################################################################
###########################################################################
### fixPDB - reads a PDB file, eliminates garbage.  Takes NO chain or A chains only.
#NOTE - this does not fix the HETATM methionines.
###########################################################################
###########################################################################
##COLUMNS      DATA TYPE        FIELD      DEFINITION
##------------------------------------------------------
## 1 -  6      Record name      "ATOM    "
## 7 - 11      Integer          serial     Atom serial number.
##13 - 16      Atom             name       Atom name.
##17           Character        altLoc     Alternate location indicator.
##18 - 20      Residue name     resName    Residue name.
##22           Character        chainID    Chain identifier.
##23 - 26      Integer          resSeq     Residue sequence number.
##27           AChar            iCode      Code for insertion of residues.
##31 - 38      Real(8.3)        x          Orthogonal coordinates for X in 
##                                         Angstroms
##39 - 46      Real(8.3)        y          Orthogonal coordinates for Y in 
##                                         Angstroms
##47 - 54      Real(8.3)        z          Orthogonal coordinates for Z in 
##                                         Angstroms
##55 - 60      Real(6.2)        occupancy  Occupancy.
##61 - 66      Real(6.2)        tempFactor Temperature factor.
##77 - 78      LString(2)       element    Element symbol, right-justified.
##79 - 80      LString(2)       charge     Charge on the atom.
##


###########################################################################
###########################################################################
### View Chains
###########################################################################
###########################################################################
def viewChains(pdbFiles):
	
#	print pdbFiles;
	
	for pdbFileName in pdbFiles:
		pdbFile = open(pdbFileName, 'rt');
		line = pdbFile.readline();

		currentChain = "@";
		chainStart = -1;
		chainStarted = False;
		lastResidueNumber = -1;

		while line:
			if line.startswith("ATOM  "):

				thisChain = line[21:22];
				thisRes = int(line[22:26].split()[0]);

				if currentChain != thisChain and not chainStarted:
					currentChain = thisChain;
					chainStart = thisRes;
					chainStarted = True;
				
				if currentChain != thisChain and chainStarted:
					print("FILE: " + pdbFileName + " Chain: "+ currentChain + " Start: " + str(chainStart) + " Stop: "+ str(lastResidueNumber) );
					currentChain = thisChain;
					chainStart = thisRes;

				lastResidueNumber = thisRes;
			
			line = pdbFile.readline();

		print("FILE: " + pdbFileName  + " Chain: "+ currentChain + " Start: " + str(chainStart) + " Stop: "+ str(lastResidueNumber) );
	
		pdbFile.close();




###########################################################################
###########################################################################
### File gobble
###########################################################################
###########################################################################
def gobble(fileName):
	#print("Gobbling: "+ fileName);
	
	inputFile = open(fileName, 'rt');
	inputLine = inputFile.readline();
	
	outputList = "".split();  #### list declaration
	
	while inputLine:
		outputList.append(inputLine);
		inputLine = inputFile.readline();

	inputFile.close();
	
#	for l in outputList:
#		print l;
	
	return outputList;



###########################################################################
###########################################################################
### View Chains
###########################################################################
###########################################################################
def editChains(inputFileName, keep):
	
	inputFile = open(inputFileName, 'rt');
	inputLine = inputFile.readline();
	
	while inputLine:
		inputList = inputLine.split();
		pdbFileName = inputList[1];
		pdbChain = inputList[3];
		chainStart = int(inputList[5]);
		chainEnd = int(inputList[7]);
		
		#check to make sure the PDB file exists.  If not, skip.
		if not os.path.exists(pdbFileName):
			print("Filename [" + pdbFileName + "] does not exist.  Skipping\n");
			inputLine = inputFile.readline();
			continue;
		
		#print pdbFileName;
		#print pdbChain;
		#print chainStart;
		#print chainEnd;

		pdbFileList = gobble(pdbFileName);

		outputFile = open(pdbFileName, 'wt');
		
		for line in pdbFileList:
#			print(line);
			#eliminate all non-ATOM lines.
			#if not line.startswith("ATOM  "):
				#outputFile.write(line);

			if line.startswith("ATOM  "):
				thisChain = line[21:22];
				thisRes = int(line[22:26]);
				#print("thisChain: " +thisChain + " thisRes: " + str(thisRes) + " pdbChain: " + pdbChain + " cStart: " + str(chainStart) + " cEnd: " + str(chainEnd) );

				if not keep:
					if not (thisChain == pdbChain and thisRes >= chainStart and thisRes <= chainEnd ):
						outputFile.write(line);
				if keep:  ## this is a bug; if there are multiple entries they will delete each other.
					if (thisChain == pdbChain and thisRes >= chainStart and thisRes <= chainEnd ):
						outputFile.write(line);

		outputFile.close();		

		inputLine = inputFile.readline();

	inputFile.close();


###########################################################################
###########################################################################
### filterChains
###########################################################################
###########################################################################
def filterChains(pdbFiles):
	
	for pdbFileName in pdbFiles:
		pdbFileLineList = gobble(pdbFileName);
		outputFile = open(pdbFileName, 'wt');

		for line in pdbFileLineList:
			if line.startswith("ATOM  "):
				outputFile.write(line);
	
		outputFile.close();
	

###########################################################################
###########################################################################
### unifyChains
###########################################################################
###########################################################################
def unifyChains(pdbFiles):
	
	for pdbFileName in pdbFiles:
		pdbFileLineList = gobble(pdbFileName);
		outputFile = open(pdbFileName, 'wt');

		for line in pdbFileLineList:
			#print line[0:21];
			outputFile.write(line[0:21] + "A"+ line[22::]);
	
		outputFile.close();
	
###########################################################################
###########################################################################
### separateChains
###########################################################################
###########################################################################
def separateChains(outputDir, pdbFiles):

	for pdbFileName in pdbFiles:
		if not pdbFileName.endswith(".pdb"):
			print("ERROR: non-PDB file \"" + pdbFileName + "\" skipped.");
			continue;
		
		## read through once and find the chains.
		pdbFileLineList = gobble(pdbFileName);

		for line in pdbFileLineList:
#			print line;
			if ( line.startswith("ATOM  ") ):
				myChar = line[21];
				outputFileName = outputDir + pdbFileName[0:-4] + myChar + ".pdb";
				outputFile = open(outputFileName, 'a');
				outputFile.write(line);
				outputFile.close();

###########################################################################
###########################################################################
### separateChains2
###########################################################################
###########################################################################
def separateChains2(outputDir, pdbFiles):

	for pdbFileName in pdbFiles:
		if not pdbFileName.endswith(".pdb"):
			print("ERROR: non-PDB file \"" + pdbFileName + "\" skipped.");
			continue;
		
		## read through once and find the chains.
		pdbFileLineList = gobble(pdbFileName);

		fileSet = [];
		fileHash = [];

		for line in pdbFileLineList:
			thisIndex = -1;
#			print line;
			if ( line.startswith("ATOM  ") ):
				myChar = line[21];
				
				## get the correct file set for this new chain.
				for i in range(0, len(fileHash)):
					if (fileHash[i] == myChar):
						thisIndex = i;
						break;
				
				if (thisIndex == -1):
					thisIndex = len(fileHash);
					fileHash.append(str(myChar));
					fileSet.append([]);
				
				thisList = fileSet[thisIndex];
				thisList.append(line);

		for i in range(0, len(fileSet)):
			thisChar = fileHash[i];
			outputFileName = outputDir + pdbFileName[0:-4] + thisChar + ".pdb";
			outputFile = open(outputFileName, 'w');
			thisSet = fileSet[i];
			
			for j in range(0, len(thisSet)):
				outputFile.write( thisSet[j] );

			outputFile.close();

###########################################################################
###########################################################################
### Main Method
###########################################################################
###########################################################################
def main():
	
#	testDebug();
	
	#print sys.argv[1]
	#print len(sys.argv)
	#print "Hello, World!"

	################################################ Usage Statement
	if len(sys.argv) == 1:
		usage()
		raise SystemExit, 5

	################################################ Help Statement
	if sys.argv[1] == "-view":
		
		viewChains(sys.argv[2::]);

		raise SystemExit, 5
	################################################ 

	if sys.argv[1] == "-edit":
		
		editChains(sys.argv[2], False);

		raise SystemExit, 5
	################################################ 

	if sys.argv[1] == "-filter":
		
		filterChains(sys.argv[2::]);

		raise SystemExit, 5
	################################################ 

	if sys.argv[1] == "-unify":
		
		unifyChains(sys.argv[2::]);

		raise SystemExit, 5
	################################################ 
	#print("python pdbChainProc.py -separate [outputDir] [pdbfile(s)] "  );

	if sys.argv[1] == "-separate":
		
		#separateChains(sys.argv[2], sys.argv[3::]);
		separateChains2(sys.argv[2], sys.argv[3::]);

		raise SystemExit, 5


#	if sys.argv[1] == "-keep":
#		
#		editChains(sys.argv[2], True);
#
#		raise SystemExit, 5
	################################################ 

if __name__ == "__main__":
	main()
	
	
	
