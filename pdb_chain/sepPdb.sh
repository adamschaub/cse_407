#!/bin/sh

EXT='.pdb'
DIR='chains/'
OUT='no_antibodies/'

PROTS="$(< ../pdb_list.txt)"

rm $DIR*$EXT
rm $OUT*$EXT
rm *$EXT
cp ../pdb/*$EXT .
for p in $PROTS
do
    echo 'Separating '$p$EXT
    python pdbChainProc.py -separate $DIR $p$EXT


    if [ -e $DIR/$p'H'$EXT ]
    then
        echo 'H-L exists for ' $p
        echo cat $DIR$p'H'$EXT $DIR$p'L'$EXT > $DIR$p's'$EXT
        cat $DIR$p'H'$EXT $DIR$p'L'$EXT > $DIR$p's'$EXT
    else
        if [ -e $DIR/$p'D'$EXT ]
        then
            echo 'C-D pair exists for ' $p
            cat $DIR$p'C'$EXT $DIR$p'D'$EXT > $DIR$p's'$EXT
        fi

    fi
done

mv $DIR*s.pdb $OUT
