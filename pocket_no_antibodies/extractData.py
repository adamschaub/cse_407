import sys

pocket = 'Pocket'
vol = 'Volume'
surf = "TotalSASA"
hydro = "Hydrophobicityscore"
alpha = 'Meanalphasphereradius'
alpha_num = 'NumberofAlphaSpheres'
alpha_den = 'Alphaspheredensity'
alpha_cent = 'Cent.ofmass-AlphaSpheremaxdist'

def parse_file(file_name, dir_out, cavity_nums):
    print cavity_nums
    if len(cavity_nums) == 1 and int(cavity_nums[0]) == -1:
        return;

    try:
        f = open(file_name, 'r')
    except:
        print "Couldn't open file " + file_name
        return

    num_pockets = 0
    total_volume = 0
    total_surface = 0
    total_hydro = 0
    total_alpha_den = 0
    first_volume = 0
    max_volume = 0
    collect = False
    for line in f:
        line = line.strip().split(" ")
        if line[0] == pocket:
            print "Pocket", line[1],
            collect = False
            for num in cavity_nums:
                if int(num)+1 == int(line[1]):
                    collect = True
                    print '(Collecting)'
                else:
                    print ''
            num_pockets += 1
        line = [s.strip() for s in line]
        line = "".join(line).split(":")

        header = line[0]
        if header == vol:
            print "\tVol", line[-1]
            volume = float(line[-1])
            if collect:
                total_volume += volume
            if first_volume == 0:
                first_volume = volume
            if volume > max_volume:
                max_volume = volume

        if header == surf:
            print "\tSASA", line[-1]
            surface = float(line[-1])
            if collect:
                total_surface += surface

        if header == hydro:
            print "\tHydro", line[-1]
            hydrophobicity = float(line[-1])
            if collect:
                total_hydro += hydrophobicity

        if header == alpha_den:
            print "\tAlpha Density", line[-1]
            alpha_density = float(line[-1])
            if collect:
                total_alpha_den += alpha_density

    print num_pockets, 'pockets'
    print total_volume, 'pocket volume'
    print total_surface, 'pocket surface'
    print total_surface/total_volume, 'pocket surf/vol'
    print total_hydro/len(cavity_nums), 'hydrophobicity'
    print total_alpha_den/len(cavity_nums), 'density'

    out_filebase = dir_out + '/' + file_name[0:4] + '_'
    try:
        out_file = out_filebase + 'cavity.dat'
        fout = open(out_file, 'w')
    except:
        print "Couldn't open filename " + out_file
        return
    print 'Writing to', out_file
    fout.write('{0} {1} {2} {3} {4} {5}'.format(num_pockets, total_volume, total_surface, total_surface/total_volume, total_hydro/len(cavity_nums), total_alpha_den/len(cavity_nums)))
"""  if header == alpha:
            print "\tAlpha", line[-1]
        if header == alpha_num:
            print "\tNum alpha spheres", line[-1]
        if header == alpha_cent:
            print "\tAlpha centroid", line[-1]
        if header == alpha_den:
            print "\tAlpha sphere density", line[-1]
"""
#----------MAIN----------#
if len(sys.argv) < 4:
    print 'USAGE: python extractData.py <PROTEIN_info.txt> <OUT_DIR> <CAVITY_NUMS>'
else:
    #pockets are zero-indexed when passed in
    parse_file(sys.argv[1], sys.argv[2], sys.argv[4:])
