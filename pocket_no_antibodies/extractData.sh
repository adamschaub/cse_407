#!/bin/sh

URL='http://www.rcsb.org/pdb/files/'
EXT='.pdb'
DIR='pdb'

PROTS="$(< ../pdb_list.txt)"
for p in $PROTS
do
    echo $p
    CAV_NUM="$(grep $p ../pocket_match.txt)"
    python extractData.py $p's_out'/$p's_info.txt' ../correlate/data/ $CAV_NUM
done
