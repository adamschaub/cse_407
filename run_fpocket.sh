#!/bin/sh/

if [ $# -lt 2 ]; then
    echo "Please provide output directory for fpocket"
    echo "sh run_fpocket.sh <INPUT_DIR> <OUTPUT_DIR>"
else

DIR_IN=$1
DIR_OUT=$2

rm -r $DIR_OUT/*s_out

for g in $DIR_IN/*.pdb
do
    name=$(basename "$g" ".pdb")
    echo "fpocket cavity search: $g -> $DIR_OUT/${name}_out"
    fpocket -f $g
    mv $DIR_IN/${name}_out $DIR_OUT
done
fi
